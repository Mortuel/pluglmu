package statemachines;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.lucci.lmu.script.PlugToLmu;

public class SMToNuSMVHandler extends AbstractHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection is = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		if (!(is instanceof StructuredSelection)) return null;
		
		String s = ((StructuredSelection)is).toString();
		IStructuredSelection iss = (IStructuredSelection) is;
		Object element = iss.getFirstElement();
		
		URL workspacePath = null;
		String outputBuildPath = null;
		ICompilationUnit icu = null;
		icu = (ICompilationUnit) element;
		
		try {
			outputBuildPath = icu.getJavaProject().getOutputLocation().lastSegment();
		} catch (JavaModelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		IProject p = (IProject) icu.getJavaProject().getProject();
		
		try {
			workspacePath = p.getLocationURI().toURL();
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String filePath = workspacePath.getPath();
		String elementName =  icu.getElementName();
		
		// For testing if working
		FileWriter fw = null;
		File f = new File("/Users/Mortuel/Documents/TMP/test.txt");
		try {
			fw = new FileWriter(f);
			fw.write(filePath + "/" + elementName + "\n");
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new PlugToLmu(filePath + "/" + elementName,
			      "/Users/Mortuel/Documents/TMP/",
			      "org3",
			      "jpg").generateFromJava("org.lucci.lmu.Attributes");
		
		
		return null;
	}
}