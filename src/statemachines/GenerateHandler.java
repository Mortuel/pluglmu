package statemachines;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.CompilationUnit;
import org.eclipse.jdt.internal.core.PackageFragment;
import org.eclipse.jdt.internal.core.PackageFragmentRoot;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;

public class GenerateHandler extends AbstractHandler implements IHandler {

	private URL workspacePath = null;
	private String format;
	private String outputPreferencePath;
	private String outputBuildPath;

	private Map<String, String> paths;

	// -------------------------- //
	// --- Semantic execution --- //
	// -------------------------- //

	public GenerateHandler() {
		super();
		initEnvironment();
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		paths = new HashMap<>();

		IStructuredSelection sSelection = getSelection();
		if (sSelection == null)
			return null;

		if (workspacePath == null)
			searchPathsProperties(sSelection);

		initPreferences();

		for (Object element : sSelection.toArray()) {
			treatObject(element);
		}

		if (!paths.isEmpty()) {
			String name = findName(paths.values());
			List<Class<?>> classes = loadClasses();
			String output = outputPreferencePath + "/" + name;
			output.replace("\\", "/");
			JOptionPane.showMessageDialog(null, "Exported as "
					+ output + "." + format);
		}

		return null;
	}

	private List<Class<?>> loadClasses() {

		URL[] urlTab = new URL[1];
		try {
			String urlPath = workspacePath.toExternalForm() + "/" + outputBuildPath + "/";
			urlPath.replace("\\", "/");
			urlTab[0] = new URL(urlPath);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		ClassLoader classLoader = new URLClassLoader(urlTab);

		List<Class<?>> classes = new ArrayList<>();

		for (Entry<String, String> classInfo : paths.entrySet()) {
			String perfectName = classInfo.getValue() + "." + "LOL";
			try {
				Class currentClass = classLoader.loadClass(perfectName);
				classes.add(currentClass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return classes;
	}

	// ------------------ //
	// --- Treatments --- //
	// ------------------ //

	private String findName(Collection<String> values) {
		String finalName = (String) values.toArray()[0];
		for (String s : values) {
			if (finalName.contains(s))
				finalName = s;
		}
		for (String s : values) {
			if (!s.contains(finalName)) {
				return "retro";
			}
		}
		return "retro-" + finalName;
	}

	private void treatObject(Object o) {

		try {
			GenerateHandler.class.getMethod("treat", o.getClass()).invoke(this,
					o);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			try {
				treat((IFile) o);
			} catch (Throwable t) {
				JOptionPane
						.showMessageDialog(null,
								"Sounds like it's a 'JarPackageFragmentRoot' : can't analyse it.");
			}
		}
	}

	private void treat(IFile jarFile) {
		try {
			String jarLocation = jarFile.getLocation().toPortableString();
			String outputName = outputPreferencePath.replace("\\", "/") + "/" + "LOL";

			JOptionPane.showMessageDialog(null, "Exported as '" + outputName
					+ "." + format + "'");


		} catch (Throwable t) {
			JOptionPane.showMessageDialog(null, t.getStackTrace());
			t.printStackTrace();
		}

	}

	public void treat(PackageFragment ipk) throws JavaModelException {
		for (Object packageChild : ipk
				.getChildrenOfType(PackageFragmentRoot.PACKAGE_FRAGMENT))
			treat((PackageFragment) packageChild);
		for (Object child : ipk
				.getChildrenOfType(PackageFragment.COMPILATION_UNIT))
			treat((CompilationUnit) child);

	}

	public void treat(CompilationUnit cu) {
		treatCU((IJavaElement) cu, cu.getParent().getElementName());
	}

	private void treatCU(IJavaElement res, String packageName) {
		try {
			String resPath = res.getPath().removeFirstSegments(2)
					.removeFileExtension().addFileExtension("class")
					.toPortableString();
			String resFinalPath = (workspacePath + "/" + outputBuildPath + "/" + resPath)
					.replace("/", "\\");

			paths.put(resFinalPath, packageName);

		} catch (Throwable t) {
			JOptionPane
					.showMessageDialog(null, "Can't find .class files :" + t);
		}
	}

	private void treat(IJavaElement res, String packageName) {

		try {
			String resPath = res.getPath().removeFirstSegments(2)
					.toPortableString();
			String resFinalPath = (workspacePath + "/" + outputBuildPath + "/" + resPath)
					.replace("/", "\\");

			paths.put(resFinalPath, packageName);

		} catch (Throwable t) {
			JOptionPane
					.showMessageDialog(null, "Can't find .class files :" + t);
		}

	}

	// ----------------------- //
	// --- Initializations --- //
	// ----------------------- //

	private void initPreferences() {

		/*format = Activator.getDefault().getPreferenceStore()
				.getString(FormatPreferences.PREF_LMU_EXPORT_FORMAT);
		outputPreferencePath = Activator.getDefault().getPreferenceStore()
				.getString(FormatPreferences.PREF_LMU_EXPORT_PATH);*/

	}

	private void initEnvironment() {

		//controller = new LmuCoreController();

	}

	private IStructuredSelection getSelection() {

		ISelection selection;
		paths = new HashMap<>();

		selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getSelection();

		if (selection == null) {
			return null;
		}

		searchPathsProperties(selection);

		return (IStructuredSelection) selection;
	}

	private void searchPathsProperties(ISelection sel) {

		IStructuredSelection ss = (IStructuredSelection) sel;
		Object element = ss.getFirstElement();

		IPackageFragmentRoot ipkr = null;
		IPackageFragment ipk = null;
		ICompilationUnit icu = null;

		try {
			ipkr = (IPackageFragmentRoot) element;
		} catch (Throwable t) {
		}
		try {
			ipk = (IPackageFragment) element;
		} catch (Throwable t) {
		}
		try {
			icu = (ICompilationUnit) element;
		} catch (Throwable t) {
		}

		IProject p = null;
		if (ipkr != null) {
			try {
				outputBuildPath = ipk.getJavaProject().getOutputLocation()
						.lastSegment();
			} catch (JavaModelException | NullPointerException e) {
				e.printStackTrace();
			}
			p = (IProject) ipkr.getJavaProject().getProject();
		} else if (ipk != null) {
			try {
				outputBuildPath = ipk.getJavaProject().getOutputLocation()
						.lastSegment();
			} catch (JavaModelException | NullPointerException e) {
				e.printStackTrace();
			}
			p = (IProject) ipk.getJavaProject().getProject();
		} else if (icu != null) {
			try {
				outputBuildPath = icu.getJavaProject().getOutputLocation()
						.lastSegment();
			} catch (JavaModelException | NullPointerException e) {
				e.printStackTrace();
			}
			p = (IProject) icu.getJavaProject().getProject();
		}

		if (p != null)
			try {
				workspacePath = p.getLocationURI().toURL();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
	}
}