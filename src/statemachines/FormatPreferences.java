package statemachines;

import lmu.Activator;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class FormatPreferences extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public static final String PREF_LMU_EXPORT_FORMAT = "PREF_LMU_EXPORT_FORMAT";
	public static final String PREF_LMU_EXPORT_PATH = "PREF_LMU_EXPORT_PATH";
	
	public FormatPreferences() {
        super(GRID);
    }
	
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	public void createFieldEditors() {
        Composite parent = getFieldEditorParent();
        addField(new ComboFieldEditor(
        		PREF_LMU_EXPORT_FORMAT, "Export format", new String[][] {
				{"PDF", "pdf"},
				{"LMU", "lmu"},
				{"DOT", "dot"},
				{"JAVA", "java"},
				{"PS", "ps"},
				{"PNG", "png"},
				{"FIG", "fig"},
				{"SVG", "svg"}
			},
          parent));
        addField(new DirectoryFieldEditor(PREF_LMU_EXPORT_PATH, "Export path", parent));
       
    }
}