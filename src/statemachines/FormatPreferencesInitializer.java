package statemachines;

import lmu.Activator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

public class FormatPreferencesInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {

		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
        store.setDefault(FormatPreferences.PREF_LMU_EXPORT_FORMAT, "PDF");
        store.setDefault(FormatPreferences.PREF_LMU_EXPORT_PATH, System.getProperty("user.dir"));
	
	}

}