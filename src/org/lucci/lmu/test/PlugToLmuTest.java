package org.lucci.lmu.test;

import java.io.IOException;


import org.lucci.lmu.input.ParseError;
import org.lucci.lmu.script.PlugToLmu;

public class PlugToLmuTest {

	public static void main(String... args) throws ParseError, IOException
	{
		//exemple pour un fichier jar
		//arguments : chemin vers le fichier, chemin vers le fichier de sortie, nom et extension
		new PlugToLmu("/Users/Mortuel/Documents/TMP/lmu-0.1.1/org.jar",
					  "/Users/Mortuel/Documents/TMP/lmu-0.1.1/",
					  "org").generateFromJar();
		
		//exemple pour un fichier java
		//arguments chemin vers le fichier, chemin vers le fichier de sortie, nom et extension
		//arguments de generate from java , le package + le nom comme indiqué
		new PlugToLmu("/Users/Mortuel/Documents/TMP/LOLO/Truc.java",
				      "/Users/Mortuel/Documents/TMP/",
				      "org3",
				      "jpg").generateFromJava("org.lucci.lmu.Attributes");

	}
}
