package org.lucci.lmu.script;

import java.io.IOException;

import org.lucci.lmu.Model;
import org.lucci.lmu.input.JarFileAnalyser;
import org.lucci.lmu.input.JavaFileAnalyser;
import org.lucci.lmu.input.LmuParser;
import org.lucci.lmu.input.ModelException;
import org.lucci.lmu.input.ModelFactory;
import org.lucci.lmu.input.ParseError;
import org.lucci.lmu.output.AbstractWriter;
import org.lucci.lmu.output.WriterException;

import toools.io.FileUtilities;
import toools.io.file.RegularFile;

public class PlugToLmu {
	
	private final String inputPath;
	private final String outputExtension;
	private final String outputPath;
	private final String fileName;
	
	public PlugToLmu(String inputPath){
		this(inputPath,"/tmp/","output" ,"pdf");

	}
	
	public PlugToLmu(String inputPath, String outputPath,String fileName){
		this(inputPath,outputPath,fileName,"pdf");
	}
	
	public PlugToLmu(String inputPath, String outputPath, String fileName, String output){
		this.inputPath = inputPath;
		this.outputExtension = output;
		this.outputPath = outputPath;
		this.fileName = fileName;
	}
	
	public void generateFromJava(String packageName){
		final String input = "java";
		JavaFileAnalyser model = new JavaFileAnalyser();
		model.setPackageName(packageName);
		toLmu(inputPath, input, model);
	}
	
	public void generateFromJar(){
		final String input = "jar";
		toLmu(inputPath, input, new JarFileAnalyser());
	}
	
	public void generateFromLmu(){
		final String input = "lmu";
		toLmu(inputPath, input, LmuParser.getParser());
	}
	
	private void toLmu(String jarPath, String inputType, ModelFactory modelFactory){
		
		
		RegularFile inputFile = new RegularFile(jarPath);
		
		try
		{	
			if (modelFactory == null)
			{
				System.err.println("No parser defined for input type '" + inputType + "\n");
			}
			else
			{
				RegularFile outputFile = new RegularFile(outputPath+fileName+"."+outputExtension);
				String outputType = FileUtilities.getFileNameExtension(outputFile.getName());
				AbstractWriter factory = AbstractWriter.getTextFactory(outputType);

				if (factory == null)
				{
					System.err.println("Do not know how to generate '" + outputType + "' code\n");
				}
				else
				{
					byte[] inputData = inputFile.getContent();
					Model model = modelFactory.createModel(inputData);

					try
					{
						byte[] outputBytes = factory.writeModel(model);
						outputFile.setContent(outputBytes);
					}
					catch (WriterException ex)
					{
						System.err.println(ex.getMessage() + "'\n");
					}
					catch (IOException ex)
					{
						System.err.println("I/O error while writing file " + outputFile.getPath() + "\n");
					}
				}
			}
		}
		catch (ParseError ex)
		{
			System.err.println("Parse error: " + ex.getMessage() + "\n");
		}
		catch (ModelException ex)
		{
			System.err.println("Model error: " + ex.getMessage() + "\n");
		}
		catch (IOException ex)
		{
			System.err.println("I/O error: " + ex.getMessage() + "\n");
		}
	}
}
